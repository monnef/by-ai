const CACHE_NAME = 'elegant-timer-v1.4';
const ASSETS_TO_CACHE = [
    '.',
    'index.html',
    'https://cdn.jsdelivr.net/npm/lucide-static@0.294.0/font/lucide.min.css',
    'https://unpkg.com/lucide@latest/dist/umd/lucide.js',
    'https://fonts.googleapis.com/css2?family=Space+Grotesk:wght@300;500;700&display=swap'
];

const networkFirstStrategy = async (request) => {
    try {
        // Always try network first for all requests
        const networkResponse = await fetch(request);
        
        // Only cache successful responses, excluding chrome-extension schema
        if (networkResponse.status === 200 && 
            networkResponse.type === 'basic' && 
            request.url.indexOf('chrome-extension://') === -1) {
            const cache = await caches.open(CACHE_NAME);
            cache.put(request, networkResponse.clone());
        }
        
        return networkResponse;
    } catch {
        // Fallback to cache if network fails
        const cachedResponse = await caches.match(request);
        return cachedResponse || new Response(null, { status: 404 });
    }
};

self.addEventListener('install', (event) => {
    // Skip waiting to immediately activate the new service worker
    self.skipWaiting();
    
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then((cache) => cache.addAll(ASSETS_TO_CACHE))
            .catch((error) => console.error('Cache installation failed:', error))
    );
});

self.addEventListener('activate', (event) => {
    // Claim all clients to ensure the new service worker takes control immediately
    event.waitUntil(self.clients.claim());
    
    // Delete all old caches
    event.waitUntil(
        caches.keys().then((cacheNames) => Promise.all(
            cacheNames
                .filter((name) => name !== CACHE_NAME)
                .map((name) => caches.delete(name))
        ))
    );
});

self.addEventListener('fetch', (event) => {
    event.respondWith(networkFirstStrategy(event.request));
}); 