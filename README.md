Hey there! 👾

Welcome to a wild collection of mini-projects 🔮 cooked up by AI, with humans mostly just along for the ride. 🎠🙂

These digital creations 📟 were typically whipped up on [Perplexity](https://perplexity.ai) using the handy [AIlin](https://gitlab.com/monnef/ailin) tool. 🧰

# [by-ai](https://monnef.gitlab.io/by-ai) 🤖
Where machines do the heavy lifting, and we humans just hit "run" 🚀✨
