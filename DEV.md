# Project Overview

This project is a collection of various tools and applications developed under the [By AI](https://gitlab.com/monnef/by-ai) initiative. Each project is organized in its own directory, structured by year, containing an `index.html` file for presentation and a `stats.yaml` file for metadata.

## Stats File

Each `stats.yaml` file includes the following fields:

### `name`
The name of the project.
- Examples:
  - "Markdown Escaper"
  - "FFXIV Dawntrail Countdown"
  - "Bovine Buddy"

### `date`
The date of the last update in YYYY-MM-DD format.
- Examples:
  - "2024-08-15"
  - "2024-06-23"
  - "2024-10-10"

### `time`
The estimated time taken to complete the project (e.g., "15min").
- Examples:
  - "2min"
  - "30min"
  - "1h"

### `prompts`
The number of prompts used in the project.
- Examples:
  - 1
  - 20
  - 8

### `rewrites`
The number of prompt rewrites made during development.
- Examples:
  - 0
  - 5
  - 3

### `model`
The AI model utilized for the project (e.g., "Claude 3.5 Sonnet").
- Examples:
  - "Claude 3.5 Sonnet"
  - "Nous: Hermes 3 405B Instruct"

### `platform`
The platform on which the project is based (e.g., "Perplexity + AIlin").
- Examples:
  - "Perplexity + AIlin"
  - "Open WebUI"

### `interaction`
A description of the interaction style used in the project (optional).
- Examples:
  - "no programming help from human"
  - "only visual observations, no help with code"

### `notes`
Any additional notes related to the project (optional).
- Example:
  - "Unexpected problems with handling of unicode characters in JavaScript."

## Directory Structure

```
./<year>/<project-name>
```

### Example

```
.
├── 2024
│   ├── 2b-calc
│   │   ├── index.html
│   │   └── stats.yaml
│   ├── cyberpunk-pexeso
│   │   ├── index.html
│   │   └── stats.yaml
```

## Recommended footer

```
<footer class="footer">
    made by Claude 3.7 Sonnet on Perplexity with <a href="https://gitlab.com/monnef/ailin">AIlin</a>, with no help from human monnef | 
    <a href="https://monnef.gitlab.io/by-ai">by-ai</a>
</footer>
```

It should be placed at the end of the body, styled to be subtle and small at the bottom of the page.
