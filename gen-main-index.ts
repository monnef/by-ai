#!/usr/bin/env bun
// ##BY-AI--gen-main-index##

const fs = require('fs');
const path = require('path');
const yaml = require('js-yaml');
const ejs = require('ejs');

const envBoolStringToBool = (value?: string): boolean => {
  if (!value) return false;
  const normalizedValue = value.toLowerCase().trim();
  return ['1', 'true', 'on'].includes(normalizedValue);
};

const REPO_URL = 'https://gitlab.com/monnef/by-ai';
const WEBSITE_URL = 'https://monnef.gitlab.io/by-ai';
const LOCAL = envBoolStringToBool(process.env.LOCAL);

const ellipsize = (str = '', maxLength) => 
  str.length > maxLength ? `${str.slice(0, maxLength - 3)}…` : str;

async function main() {
  const projectDirs = await findProjectDirs('.');
  const projectInfo = await Promise.all(projectDirs.map(getProjectInfo));
  const sortedProjects = projectInfo.sort((a, b) => {
    if (a.date && b.date) return new Date(b.date) - new Date(a.date);
    if (a.date) return -1;
    if (b.date) return 1;
    return 0;
  });
  await writeIndex(sortedProjects);
  console.log("Main index.html has been generated.");
}

async function findProjectDirs(dir) {
  const contents = await fs.promises.readdir(dir);
  const projectDirs = [];

  for (const item of contents) {
    if (item === '.git') continue;
    const itemPath = path.join(dir, item);
    const isDir = await fs.promises.stat(itemPath).then(stat => stat.isDirectory()).catch(() => false);
    if (isDir) {
      const subContents = await fs.promises.readdir(itemPath);
      for (const subItem of subContents) {
        const subItemPath = path.join(itemPath, subItem);
        const subIsDir = await fs.promises.stat(subItemPath).then(stat => stat.isDirectory()).catch(() => false);
        if (subIsDir) {
          const indexPath = path.join(subItemPath, 'index.html');
          const exists = await fs.promises.access(indexPath).then(() => true).catch(() => false);
          if (exists) {
            projectDirs.push(path.join(item, subItem));
          }
        }
      }
    }
  }

  return projectDirs;
}

async function getProjectInfo(dir) {
  const statsFile = path.join('.', dir, 'stats.yaml');
  const stats = await parseYaml(statsFile);
  const formattedDate = stats.date ? new Date(stats.date).toISOString().split('T')[0] : null; // Format date as YYYY-MM-DD
  return { 
    name: stats.name || path.basename(dir),
    year: path.dirname(dir),
    date: formattedDate,
    model: stats.model,
    platform: stats.platform,
    dir: dir, // Add this line to include the original directory path
  };
}

async function parseYaml(file) {
  try {
    const content = await fs.promises.readFile(file, 'utf8');
    return yaml.load(content);
  } catch (err) {
    return {};
  }
}

const formatDate = (dateString) => {
  const date = new Date(dateString);
  return date.toISOString().split('T')[0]; // Format date as YYYY-MM-DD
};

interface StatsFile {
    name: string;
    date: string | null; // Format as YYYY-MM-DD or null if not available
    time?: string; // Optional, e.g., "2min", "30m"
    prompts: number; // Number of prompts
    rewrites: number; // Number of rewrites
    model: string; // Model name
    platform: string; // Platform name
    interaction?: string; // Optional interaction description
    notes?: string; // Optional notes
}

async function parseDateFromYaml(file) {
  try {
    const content = await fs.promises.readFile(file, 'utf8');
    const data = yaml.load(content);
    return data.date ? formatDate(data.date) : null;
  } catch {
    return null;
  }
}

async function writeIndex(projects) {
  const template = await fs.promises.readFile('index-template.ejs', 'utf8');

  const content = ejs.render(template, {
    projects, 
    WEBSITE_URL, 
    REPO_URL,
    LOCAL,
    formatDate, 
    ellipsize, 
    path,
    encodeURIComponent 
  });

  await fs.promises.writeFile('index.html', content);
}

main().catch(err => console.error(err));
